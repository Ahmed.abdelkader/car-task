import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PartnersView extends StatefulWidget {
  @override
  _PartnersViewState createState() => _PartnersViewState();
}

class _PartnersViewState extends State<PartnersView> {
  Widget _appBar() {
    return Container(
      height: 220,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/car1.jpeg"), fit: BoxFit.fill)),
      alignment: Alignment.topCenter,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(
            Icons.menu,
            color: Colors.white,
            size: 30,
          ),
          Container(
              width: 35,
              height: 40,
              child: Stack(
                children: [
                  Positioned(
                      left: 0,
                      top: 4,
                      child: Icon(Icons.notifications,
                          color: Colors.white, size: 30)),
                  Positioned(
                    right: 0,
                    child: Container(
                      padding: const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.redAccent),
                      child: Text(
                        "12",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }

  Widget _tabBarCarType() {
    //may be change it depends on the logic and endpoints to improve it.

    return Container(
      height: 75,
      color: Colors.white,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (_, index) {
          return Padding(
            padding: const EdgeInsets.all(4.0),
            child: Column(
              children: [
                Image.asset(
                  "assets/images/sedan.png",
                  width: 80,
                  height: 40,
                  fit: BoxFit.fill,
                ),
                Text("Sedan")
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _tabBarCarMark() {
    //may be change it depends on the logic and endpoints to improve it.
    return Container(
      height: 80,
      color: Colors.white,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (_, index) {
          return Padding(
            padding: const EdgeInsets.all(4.0),
            child: Card(
              elevation: 4,
              margin: const EdgeInsets.only(bottom: 5),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Image.asset(
                    "assets/images/mark.png",
                    width: 80,
                    height: 50,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _headerForEveryItem(
      {@required String title, @required bool all, @required Function onTab}) {
    return Container(
      padding: const EdgeInsets.only(left: 16,right: 16,top: 10),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "$title",
            style: TextStyle(color: Colors.black, fontSize: 15),
          ),
          Visibility(
            visible: all,
            child: Text(
              "الكل",
              style: TextStyle(color: Colors.black, fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }

  Widget _itemDetail({String img, String price, String name}) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5,vertical: 10),
      height: 180,
      width: 250,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),

          image: DecorationImage(
              image: AssetImage("assets/images/$img"), fit: BoxFit.fill)),
      alignment: Alignment.bottomCenter,
      child: Card(
        margin: const EdgeInsets.all(0),
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)
          ),
        ),
        child: Container(
          width: 250,
          height: 50,
          decoration: BoxDecoration(

            color: Colors.white,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)
              ),

          ),
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "$name",
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
              Text(
                "تبدأ من $price ك.د",
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Container(
        color: Colors.grey.withOpacity(.3),
        child: ListView(
          children: [
            //appBar
            _appBar(),
            // tabBarCArType
            _headerForEveryItem(
                title: "تصفح حسب نوع السيارة", onTab: () {}, all: false),
            _tabBarCarType(),
            SizedBox(height: 10,),
            // tabBarCarMark
            _headerForEveryItem(
                title: "تصفح حسب الماركة", onTab: () {}, all: true),
            _tabBarCarMark(),

          //  الوكالات
            SizedBox(height: 10,),
            _headerForEveryItem(
                title: "جديد الوكالات", onTab: () {}, all: true),
            Container(
              height: 250,
              color: Colors.white,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (_, index) {
                  return Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: _itemDetail(
                        img: "car5.jpeg",
                        name: "Audi A8",
                        price: "12900"
                    )
                  );
                },
              ),

            ),


            //  videos
            SizedBox(height: 10,),
            _headerForEveryItem(
                title: "الفيديو", onTab: () {}, all: true),
            Container(
              height: 250,
              color: Colors.white,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (_, index) {
                  return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: _itemDetail(
                          img: "car5.jpeg",
                          name: "Audi A8",
                          price: "12900"
                      )
                  );
                },
              ),

            ),
          ],
        ),
      ),
    );
  }
}
