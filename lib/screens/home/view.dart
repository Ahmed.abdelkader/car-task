

import 'package:car_task/screens/partners/view.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _classState();
  }
}

class _classState extends State<Home> {
  GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();
  int _index = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final _items = [
      PartnersView(),
      Container(),
      Container(),
      Container(),
    ];

    return Directionality(
      textDirection: TextDirection.rtl,
      child: DefaultTabController(
          initialIndex: _index,
          length: 4,
          child: Scaffold(
            key: _scaffold,
            backgroundColor: Colors.white,
            body: Container(
              child: _items.elementAt(_index),
            ),
            bottomNavigationBar: BottomNavigationBar(
              backgroundColor: Color(0xffF2F2F2),
              showUnselectedLabels: true,
              elevation: 0.0,
              showSelectedLabels: true,
              type: BottomNavigationBarType.fixed,
              selectedItemColor: Colors.black,
              selectedLabelStyle: TextStyle(fontSize: 14, fontFamily: "cairo"),
              unselectedItemColor: Colors.grey,
              unselectedLabelStyle: TextStyle(fontSize: 12, fontFamily: "cairo"),
              currentIndex: _index,
              iconSize: 30,
              onTap: (position) => setState(() {
                _index = position;
              }),
              items: [
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.car_rental,
                    size: 20,
                  ),
                  title: Text(
                    'الوكلاء',
                  ),
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.shop,
                    size: 20,
                  ),
                  title: Text(
                    'الخدمات',
                  ),
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.payment,
                    size: 20,
                  ),
                  title: Text(
                    'التثمين',
                  ),
                ),

                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.save,
                    size: 20,
                  ),
                  title: Text(
                    'التأمين',
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
